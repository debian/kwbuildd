# This file is part of kwbuildd.
# See top-level LICENSE file for license information.

import argparse
import daemon
import datetime
import git
import os.path
import re
import subprocess
import sys

from kwbuildd import conf


configuration = conf.Configuration()


class Repository:
    def __init__(self, configuration, package):
        self.configuration = configuration
        self.package = package

        path = os.path.join(configuration.get_build_directory(),
                            package.get_package_name())

        self.repo = git.Repo.init(path)

        self.remote_origin = self.create_remote(
            "origin",
            package.get_git_remote_origin()
        )
        self.remote_debian = self.create_remote(
            "debian",
            package.get_git_remote_debian()
        )

    def create_remote(self, name, url):
        if url:
            try:
                remote = self.repo.create_remote(name, url)
            except git.exc.GitCommandError:
                remote = self.repo.remote(name)
                remote.set_url(url)
            return remote

    def fetch(self, branch, remote):
        if remote:
            remote.fetch()
            ref = self.repo.create_head(
                branch,
                commit=remote.refs[branch],
                force=True
            )
        else:
            ref = self.repo.heads[branch]
        return ref

    def fetch_upstream(self):
        return self.fetch(self.package.get_git_upstream_branch(),
                          self.remote_origin)

    def fetch_merge_debian(self):
        return self.fetch(self.package.get_git_merge_debian_branch(),
                          self.remote_debian)

    def fetch_build_debian(self):
        return self.fetch(self.package.get_git_build_debian_branch(),
                          self.remote_debian)

    def setup_environment(self, env):
        for k, v in [
            ("GIT_AUTHOR_NAME", self.configuration.get_git_author_name()),
            ("GIT_AUTHOR_EMAIL", self.configuration.get_git_author_email()),
            ("GIT_COMMITTER_NAME", self.configuration.get_git_committer_name()),
            ("GIT_COMMITTER_EMAIL", self.configuration.get_git_committer_email()),
            ("DEBFULLNAME", self.configuration.get_deb_uploader_name()),
            ("DEBEMAIL", self.configuration.get_deb_uploader_email()),
        ]:
            if v is not None:
                env[k] = v

    def do_nightly_merge(self):
        upstream_branch = self.package.get_git_upstream_branch()
        merge_debian_branch = self.package.get_git_merge_debian_branch()
        build_debian_branch = self.package.get_git_build_debian_branch()

        upstream_ref = self.fetch_upstream()
        merge_debian_ref = self.fetch_merge_debian()

        self.repo.head.set_reference(merge_debian_ref)
        self.repo.index.reset()
        self.repo.index.checkout(force=True)
        self.repo.git.clean("-f", "-d", "-x")

        env = dict()
        self.setup_environment(env)
        old_env = self.repo.git.update_environment(**env)
        self.repo.git.merge("--no-edit", "--no-ff", upstream_branch)
        self.repo.git.update_environment(**old_env)

        old_version = deb.parsechangelog(
            os.path.join(self.repo.working_tree_dir, "debian/changelog")
        )["version"]

        version_match = re.search("^(.*?)~", old_version)
        new_version = version_match.group(1) + "~nightly" \
            + datetime.date.today().strftime("%Y%m%d") + "-1"

        env = os.environ.copy()
        self.setup_environment(env)

        p = subprocess.run(
            [
                "gbp", "dch", "--auto", "--commit", "--debian-branch",
                merge_debian_branch, "--new-version", new_version,
                "--distribution", "unstable",
            ],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE, cwd=self.repo.working_tree_dir,
            check=True,
            env=env,
        )

        if self.remote_debian:
            self.remote_debian.push("{0}:{0}".format(merge_debian_branch))
            build_debian_branch = self.package.get_git_build_debian_branch()
            if build_debian_branch != merge_debian_branch:
                self.remote_debian.push("{0}:{1}".format(merge_debian_branch,
                                                         build_debian_branch))

    def do_nightly_build(self):
        upstream_branch = self.package.get_git_upstream_branch()
        build_debian_branch = self.package.get_git_build_debian_branch()

        upstream_ref = self.fetch_upstream()
        build_debian_ref = self.fetch_build_debian()

        self.repo.head.set_reference(build_debian_ref)
        self.repo.index.reset()
        self.repo.index.checkout(force=True)
        self.repo.git.clean("-f", "-d", "-x")

        builder = self.package.get_builder()
        args = [
            "gbp", "buildpackage",
            "--git-debian-branch=" + build_debian_branch,
            "--git-upstream-tree=" + upstream_branch,
            "--git-builder=" + builder,
        ]

        if builder == "debuild":
            args += ["-sa", "-us", "-uc"]
        elif builder == "sbuild":
            args += ["-s", "--force-orig-source"]

        try:
            p = subprocess.run(
                args, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE, cwd=self.repo.working_tree_dir,
                check=True
            )
        except BaseException as e:
            import pdb
            pdb.set_trace()

        if self.package.get_dupload_destination():
            changelog = deb.parsechangelog(
                os.path.join(self.repo.working_tree_dir, "debian/changelog")
            )
            filename_regex = "^" + re.escape(changelog["source"]) + "_" \
                + re.escape(changelog["version"]) + "_.+\\.changes$"

            for filename in os.listdir(
                self.configuration.get_build_directory()
            ):
                if re.search(filename_regex, filename):
                    deb.dupload(
                        self.package.get_dupload_destination(),
                        os.path.join(
                            self.configuration.get_build_directory(),
                            filename
                        )
                    )


class PIDFile:
    def __init__(self, filename):
        self.filename = filename

    def __enter__(self):
        try:
            with open(self.filename, "x") as f:
                print(os.getpid(), file=f)
        except FileExistsError:
            process_exists = True
            with open(self.filename, "r+") as f:
                try:
                    pid = int(f.read())
                except ValueError: # Malformed PID file?
                    process_exists = False

                if process_exists:
                    try:
                        # Signal 0 does nothing, but still does error checking
                        os.kill(pid, 0)
                    except ProcessLookupError:
                        # No process
                        process_exists = False
                    except PermissionError:
                        # Process exists, but we don't have permission to
                        # kill() it
                        pass

                if process_exists:
                    raise RuntimeError("Process already exists")
                else:
                    f.seek(0)
                    print(os.getpid(), file=f)

    def __exit__(self, exc_type, exc_value, traceback):
        os.unlink(self.filename)


class Daemon:
    def __init__(self, configuration):
        self.configuration = configuration
        self.repositories = dict()

    def init_dirs(self):
        os.makedirs(self.configuration.get_base_directory(), exist_ok=True)
        os.makedirs(self.configuration.get_build_directory(), exist_ok=True)
        os.makedirs(self.configuration.get_upload_directory(), exist_ok=True)

    def init_repos(self):
        for name, package in self.configuration.packages.items():
            self.repositories[name] = Repository(self.configuration, package)


def main():
    parser = argparse.ArgumentParser(
        description="buildd-style build daemon for continuous integration"
    )
    parser.add_argument(
        "-c", "--configuration", action="store",
        help="Configuration file to use",
        default=os.path.join(os.environ["HOME"], ".kwbuildd.conf")
    )
    parser.add_argument(
        "--no-fork", action="store_true",
        help="Do not fork into a daemon process"
    )
    parser.add_argument(
        "--do-nightly-merge", action="store",
        help="Do a nightly merge on a package (implies --no-fork)"
    )
    parser.add_argument(
        "--do-nightly-build", action="store",
        help="Do a nightly build on a package (implies --no-fork)"
    )
    args = parser.parse_args()

    try:
        configuration.read_configuration_file(args.configuration)
    except FileNotFoundError as e:
        print("Configuration file '%s' not found. Exiting."
              % args.configuration)
        exit(1)

    kwbuildd_daemon = Daemon(configuration)
    kwbuildd_daemon.init_dirs()

    if args.do_nightly_merge or args.do_nightly_build:
        args.no_fork = True

    if args.no_fork:
        daemon_stdin = sys.stdin
        daemon_stdout = sys.stdout
        daemon_stderr = sys.stderr
    else:
        daemon_stdin = None
        daemon_stdout = None
        daemon_stderr = None

    base_dir = configuration.get_base_directory()
    my_daemon = daemon.DaemonContext(
        pidfile=PIDFile(os.path.join(base_dir, "kwbuildd.pid")),
        working_directory=base_dir,
        detach_process=not args.no_fork,
        stdin=daemon_stdin,
        stdout=daemon_stdout,
        stderr=daemon_stderr,
    )
    my_daemon.open()

    kwbuildd_daemon.init_repos()

    if args.do_nightly_merge:
        kwbuildd_daemon.repositories[args.do_nightly_merge].do_nightly_merge()
    elif args.do_nightly_build:
        kwbuildd_daemon.repositories[args.do_nightly_build].do_nightly_build()
    else:
        print("Daemon mode not yet supported. Exiting.")
        exit(1)

if __name__ == "__main__":
    main()
