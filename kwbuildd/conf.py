# This file is part of kwbuildd.
# See top-level LICENSE file for license information.

import datetime
import os.path

from kwbuildd import deb


class Configuration:
    """Global configuration."""

    base_directory = None
    """Directory to use as the base for kwbuildd operations. If none is
    specified, ${HOME}/kwbuildd is used by default. Must be an absolute
    path.
    """

    build_directory = None
    """Directory to use as the base for merging and building packages.
    If none is specified, base_directory/build is used by default. If a
    relative path is specified, it is considered relative to
    base_directory.
    """

    upload_directory = None
    """Directory where packages are moved after being built in
    preparation for upload. If none is specified, base_directory/upload
    is used by default. If a relative path is specified, it is
    considered relative to base_directory.
    """

    uploader_name = None
    """Name of the person doing uploading. This is the default for
    git_author_name, git_committer_name, and deb_uploader_name. If
    neither uploader_name nor a more specific parameter is set, it is
    not changed from the user-configured default.
    """

    uploader_email = None
    """Email of the person doing the uploading. This is the default for
    git_author_email, git_committer_email, and deb_uploader_email. If
    neither uploader_email nor a more specific parameter is set, it is
    not changed from the user-configured default.
    """

    git_author_name = None
    """Name to use as the author for Git commits. If none is specified,
    the value of uploader_name is used by default.
    """

    git_author_email = None
    """Email to use as the author for Git commits. If none is specified,
    the value of uploader_email is used by default.
    """

    git_committer_name = None
    """Name to use as the committer for Git commits. If none is
    specified, the value of uploader_name is used by default.
    """

    git_committer_email = None
    """Email to use as the committer for Git commits. If none is
    specified, the value of uploader_email is used by default.
    """

    deb_uploader_name = None
    """Name to use as the uploader for the Debian changelog. If none is
    specified, the value of uploader_name is used by default.
    """

    deb_uploader_email = None
    """Email to use as the uploader for the Debian changelog. If none is
    specified, the value of uploader_email is used by default.
    """

    def __init__(self):
        self.packages = dict()

    def get_base_directory(self):
        if not self.base_directory:
            return os.path.join(os.environ["HOME"], "kwbuildd")
        assert os.path.isabs(self.base_directory)
        return self.base_directory

    def get_build_directory(self):
        if not self.build_directory:
            return os.path.join(self.get_base_directory(), "build")
        elif os.path.isabs(self.build_directory):
            return self.build_directory
        else:
            return os.path.join(self.get_base_directory(),
                                self.build_directory)

    def get_upload_directory(self):
        if not self.upload_directory:
            return os.path.join(self.get_base_directory(), "upload")
        elif os.path.isabs(self.upload_directory):
            return self.upload_directory
        else:
            return os.path.join(self.get_base_directory(),
                                self.upload_directory)

    def get_uploader_name(self):
        return self.uploader_name

    def get_uploader_email(self):
        return self.uploader_email

    def get_git_author_name(self):
        if not self.git_author_name:
            return self.get_uploader_name()
        return self.git_author_name

    def get_git_author_email(self):
        if not self.git_author_email:
            return self.get_uploader_email()
        return self.git_author_email

    def get_git_committer_name(self):
        if not self.git_committer_name:
            return self.get_uploader_name()
        return self.git_committer_name

    def get_git_committer_email(self):
        if not self.git_committer_email:
            return self.get_uploader_email()
        return self.git_committer_email

    def get_deb_uploader_name(self):
        if not self.deb_uploader_name:
            return self.get_uploader_name()
        return self.deb_uploader_name

    def get_deb_uploader_email(self):
        if not self.deb_uploader_email:
            return self.get_uploader_email()
        return self.deb_uploader_email

    def read_configuration_file(self, filename):
        """Read the configuration from a file. This file is executed as
        a Python script, with a single local variable called "conf",
        which is a reference to self.

        Parameters:
        filename -- Filename of the file to read.
        """
        with open(filename, "r") as f:
            contents = f.read()
        try:
            exec(contents, {"conf": self})
        except BaseException as e:
            raise Exception("Error reading configuration file") from e

    def add_package(self, package_name, **kwargs):
        """Add a package to the kwbuildd configuration.

        Parameters:
        package_name -- Name of the package to add.

        Keyword parameters are the same as the attributes of Package,
        with the exception of package_name.
        """
        assert package_name not in self.packages
        package = Package(package_name, **kwargs)
        self.packages[package_name] = package
        return package


class Package:
    """Configuration for a package to build."""

    package_name = None
    """Name of the Debian package to be built."""

    git_remote_origin = None
    """URL of the Git upstream remote. If none is specified, no origin
    will be created or used.
    """

    git_remote_debian = None
    """Url of the Git Debian remote. If none is specified, no Debian
    remote will be created or used.
    """

    git_upstream_branch = None
    """Name of the branch to fetch from upstream. If none is specified,
    "master" is used by default.
    """

    git_merge_debian_branch = None
    """Name of the Debian branch to merge upstream into. If none is
    specified, "debian" is used by default.
    """

    git_build_debian_branch = None
    """Name of the Debian branch to build from. If none is specified,
    git_merge_debian_branch is used by default. If a value is specified,
    nightly merges will also push to this branch.
    """

    do_nightly_merge = False
    """Whether or not to do a nightly merge. If a datetime.time is
    given, the nightly merge will be performed at that time. Otherwise,
    if a value is given that evaluates to True, the nightly merge will
    be performed at 00:00:00, local time. Otherwise, no nightly merge
    will be performed. At least one of do_nightly_merge and
    do_nightly_build must be specified.
    """

    do_nightly_build = False
    """Whether or not to do a nightly build. If a datetime.time is
    given, the nightly build will be performed at that time. Otherwise,
    if a value is given that evaluates to True, the nightly build will
    be performed at 00:01:00, local time. Otherwise, no nightly build
    will be performed.
    """

    dupload_destination = None
    """Nickname of the dupload host to dupload to. If none is
    specified, no dupload will be performed.
    """

    builder = None
    """Debian builder to use. If none is specified, sbuild will be
    used. Supported values are:

    * sbuild
    * debuild
    """

    def __init__(self, package_name, **kwargs):
        assert deb.is_valid_package_name(package_name)
        self.package_name = package_name

        for k, v in kwargs.items():
            assert k != "package_name"
            setattr(self, k, v)

    def get_package_name(self):
        return self.package_name

    def get_git_remote_origin(self):
        return self.git_remote_origin

    def get_git_remote_debian(self):
        return self.git_remote_debian

    def get_git_upstream_branch(self):
        if not self.git_upstream_branch:
            return "master"
        return self.git_upstream_branch

    def get_git_merge_debian_branch(self):
        if not self.git_merge_debian_branch:
            return "debian"
        return self.git_merge_debian_branch

    def get_git_build_debian_branch(self):
        if not self.git_build_debian_branch:
            return self.get_git_merge_debian_branch()
        return self.git_build_debian_branch

    def get_nightly_merge_time(self):
        if isinstance(self.do_nightly_merge, datetime.time):
            return self.do_nightly_merge
        elif self.do_nightly_merge:
            return datetime.time(0, 0, 0)
        else:
            return None

    def get_nightly_build_time(self):
        if isinstance(self.do_nightly_build, datetime.time):
            return self.do_nightly_build
        elif self.do_nightly_build:
            return datetime.time(0, 1, 0)
        else:
            return None

    def get_dupload_destination(self):
        return self.dupload_destination

    def get_builder(self):
        if self.builder:
            assert self.builder in {"sbuild", "debuild"}
            return self.builder
        return "sbuild"
