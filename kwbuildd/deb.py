# This file is part of kwbuildd.
# See top-level LICENSE file for license information.

import io
import re
import subprocess


def is_valid_package_name(package_name):
    return bool(re.search("^[a-z0-9][a-z0-9-+.]+$", package_name))

def parsechangelog(changelog):
    if isinstance(changelog, str):
        result = subprocess.run(
            ["dpkg-parsechangelog", "-l", changelog],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
            check=True,
        )
    else:
        input_result = changelog.read()
        if isinstance(input_result, str):
            input_result = input_result.encode("utf-8")

        result = subprocess.run(
            ["dpkg-parsechangelog", "-l", "-"],
            input=input_result,
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
            check=True,
        )

    retval = dict()
    output = io.StringIO(result.stdout.decode("utf-8"))
    for line in output:
        match = re.search("^(.*?): (.*)$", line)
        if match:
            k, v = match.groups()
            if k == "Source":
                retval["source"] = v
            elif k == "Version":
                retval["version"] = v
            elif k == "Distribution":
                retval["distribution"] = v
            elif k == "Urgency":
                retval["urgency"] = v
            elif k == "Maintainer":
                retval["maintainer"] = v

    return retval

def gbp_version():
    output = subprocess.check_output(["gbp", "--version"]).decode("utf-8")
    _, version = output.strip().split(" ")
    return [int(p) for p in version.split(".")]

def dcmd(filename):
    output = subprocess.check_output(["dcmd", filename]).decode("utf-8")
    return [f.strip() for f in io.StringIO(output)]

_dpkg_architecture_values = None

def dpkg_architecture():
    global _dpkg_architecture_values
    if not _dpkg_architecture_values:
        _dpkg_architecture_values = dict()
        output = subprocess.check_output(["dpkg-architecture"]) \
            .decode("utf-8")
        for line in io.StringIO(output):
            k, v = line.split("=", 1)
            _dpkg_architecture_values[k] = v.rstrip()
    return _dpkg_architecture_values
