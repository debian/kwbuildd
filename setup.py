#!/usr/bin/env python3

# This file is part of kwbuildd.
# See top-level LICENSE file for license information.

from setuptools import setup


setup( \
    name="kwbuildd",
    version="0.3.2",
    description="buildd-inspired build daemon for continuous integration",
    url="https://gitlab.kitware.com/debian/kwbuildd",
    author="Kyle Edwards",
    author_email="kyle.edwards@kitware.com",
    license="BSD 3 clause",
    packages=["kwbuildd"],
    entry_points={
        "console_scripts": ["kwbuildd=kwbuildd.main"],
    },
    install_requires=["gitpython", "python-daemon"],
)
