# This file is part of kwbuildd.
# See top-level LICENSE file for license information.

import datetime
import git
import gzip
import io
import lzma
import os.path
import stat
import subprocess
import tarfile
import tempfile
from unittest import TestCase
import warnings

import kwbuildd
from kwbuildd import conf, deb


CMAKELISTS_TXT = """cmake_minimum_required(VERSION 3.5)
project(kwbuildd-test C)

add_executable(kwbuildd-test main.c)
install(TARGETS kwbuildd-test RUNTIME DESTINATION bin)
"""

MAIN_C = """#include <stdio.h>

int main(int argc, char **argv) {
  printf("Hello world!\\n");
  return 0;
}
"""

README = """This is a test project
"""

DEBIAN_CHANGELOG = """kwbuildd-test (1.0~~initial-1) unstable; urgency=medium

  * Initial release

 -- Kitware Debian Maintainers <debian@kitware.com>  Sat, 21 Apr 2018 14:17:10 -0400
"""

DEBIAN_RULES = """#!/usr/bin/make -f

%:
\tdh $@ --buildsystem=cmake
"""

DEBIAN_CONTROL = """Source: kwbuildd-test
Maintainer: Kitware Debian Maintainers <debian@kitware.com>
Uploaders: Kyle Edwards <kyle.edwards@kitware.com>

Package: kwbuildd-test
Architecture: any
Description: kwbuildd test
 This is part of the kwbuildd test suite.
"""


class NightlyTestCaseBase(TestCase):
    def setUp(self):
        self.base_dir = tempfile.TemporaryDirectory()
        self.repo_dir = tempfile.TemporaryDirectory()
        self.conf = conf.Configuration()
        self.conf.base_directory = self.base_dir.name
        self.conf.git_author_name = "Author"
        self.conf.git_author_email = "author@example.com"
        self.conf.git_committer_name = "Committer"
        self.conf.git_committer_email = "committer@example.com"
        self.conf.deb_uploader_name = "Uploader"
        self.conf.deb_uploader_email = "uploader@example.com"

        self.daemon = kwbuildd.Daemon(self.conf)
        self.daemon.init_dirs()

        self.upstream = git.Repo.init(
            os.path.join(self.repo_dir.name, "upstream.git"),
            bare=True
        )
        self.debian = git.Repo.init(
            os.path.join(self.repo_dir.name, "debian.git"),
            bare=True
        )

        with tempfile.TemporaryDirectory() as gittmp_dir:
            def write_file(filename, contents, mode=None):
                path = os.path.join(gittmp_dir, filename)
                with open(path, "w") as f:
                    f.write(contents)
                if mode is not None:
                    os.chmod(path, mode)
                gittmp.index.add([filename])

            gittmp = git.Repo.init(gittmp_dir)

            gittmp.create_remote("origin", self.upstream.working_dir)
            gittmp.create_remote("debian", self.debian.working_dir)

            write_file("CMakeLists.txt", CMAKELISTS_TXT)
            write_file("main.c", MAIN_C)

            gittmp.index.commit("Initial commit")
            gittmp.remotes.origin.push("master:master")

            gittmp.create_head("refs/heads/debian")
            gittmp.head.reference = gittmp.refs.debian

            os.mkdir(os.path.join(gittmp_dir, "debian"))
            os.mkdir(os.path.join(gittmp_dir, "debian/source"))

            write_file("debian/changelog", DEBIAN_CHANGELOG)
            write_file("debian/rules", DEBIAN_RULES, mode=stat.S_IRWXU)
            write_file("debian/control", DEBIAN_CONTROL)
            write_file("debian/compat", "10\n")
            write_file("debian/source/format", "3.0 (quilt)\n")

            gittmp.index.commit("Initial Debian release")
            gittmp.remotes.debian.push("debian:debian")

            self.old_debian = gittmp.refs.debian.commit

            if isinstance(self, NightlyMergeTestCase):
                gittmp.head.reference = gittmp.refs.master

                with open(os.path.join(gittmp_dir, "README"), "w") as f:
                    f.write(README)

                gittmp.index.add(["README"])
                gittmp.index.commit("Add README")
                gittmp.remotes.origin.push("master:master")

        self.debian.head.reference = self.debian.refs.debian

        self.conf.add_package(
            "kwbuildd-test",
            git_remote_origin=self.upstream.working_dir,
            git_remote_debian=self.debian.working_dir,
            git_upstream_branch="master",
            git_merge_debian_branch="debian",
            builder="debuild",
        )

    def tearDown(self):
        self.repo_dir.cleanup()
        self.base_dir.cleanup()


class NightlyMergeTestCase(NightlyTestCaseBase):
    def check_commit_actors(self, commit):
        self.assertEqual("Author", commit.author.name)
        self.assertEqual("author@example.com", commit.author.email)
        self.assertEqual("Committer", commit.committer.name)
        self.assertEqual("committer@example.com", commit.committer.email)

    def do_test(self, check_actors=True):
        self.daemon.init_repos()
        self.daemon.repositories["kwbuildd-test"].do_nightly_merge()

        dch_commit = self.debian.refs.debian.commit
        self.assertEqual(1, len(dch_commit.parents))
        if check_actors:
            self.check_commit_actors(dch_commit)

        merge_commit = dch_commit.parents[0]
        self.assertEqual(2, len(merge_commit.parents))
        self.assertEqual(self.old_debian, merge_commit.parents[0])
        self.assertEqual(self.upstream.refs.master.commit,
                         merge_commit.parents[1])
        if check_actors:
            self.check_commit_actors(merge_commit)

        changelog_blob = dch_commit.tree["debian/changelog"]
        result = deb.parsechangelog(changelog_blob.data_stream)
        nowfmt = datetime.date.today().strftime("%Y%m%d")
        self.assertEqual("1.0~nightly%s-1" % nowfmt, result["version"])

        # This is a workaround for a known bug in versions of git-buildpackage
        # prior to 0.8.10. See Debian bug #796913 for details.
        if check_actors:
            version = deb.gbp_version()
            if version >= [0, 8, 10]:
                self.assertEqual("Uploader <uploader@example.com>",
                                 result["maintainer"])
            else:
                warnings.warn(
                    "Skipping uploader check due to a known bug in "
                    + "git-buildpackage version " + ".".join(
                        [str(p) for p in version]
                    )
                )

    def test_no_initial_repo(self):
        self.do_test()

    def test_initial_repo(self):
        repo = git.Repo.init(os.path.join(self.conf.get_build_directory(),
                                          "kwbuildd-test"))
        repo.create_remote("origin", self.upstream.working_dir)
        repo.create_remote("debian", self.debian.working_dir)
        repo.remotes.origin.fetch()
        repo.remotes.debian.fetch()

        repo.create_head("master", repo.remotes.origin.refs.master)
        repo.create_head("debian", repo.remotes.debian.refs.debian)

        self.do_test()

    def test_uploader_name_email(self):
        self.conf.git_author_name = None
        self.conf.git_author_email = None
        self.conf.git_committer_name = None
        self.conf.git_committer_email = None
        self.conf.deb_uploader_name = None
        self.conf.deb_uploader_email = None
        self.conf.uploader_name = "Maintainer"
        self.conf.uploader_email = "maintainer@example.com"

        self.do_test(False)

        dch_commit = self.debian.refs.debian.commit
        self.assertEqual("Maintainer", dch_commit.author.name)
        self.assertEqual("maintainer@example.com", dch_commit.author.email)
        self.assertEqual("Maintainer", dch_commit.committer.name)
        self.assertEqual("maintainer@example.com", dch_commit.committer.email)

    def test_trailing_commit(self):
        repo = git.Repo.init(os.path.join(self.conf.get_build_directory(),
                                          "kwbuildd-test"))
        repo.create_remote("origin", self.upstream.working_dir)
        repo.create_remote("debian", self.debian.working_dir)
        repo.remotes.origin.fetch()
        repo.remotes.debian.fetch()

        repo.create_head("master", repo.remotes.origin.refs.master)
        repo.create_head("debian", repo.remotes.debian.refs.debian)

        repo.head.set_reference(repo.heads.debian)
        repo.index.reset()
        repo.index.checkout()

        subprocess.run(
            [
                "gbp", "dch", "--auto", "--commit", "--debian-branch",
                "debian", "--new-version", "1.0~~initial+1-1",
                "--distribution", "unstable",
            ],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE, cwd=repo.working_tree_dir,
            check=True,
        )

        self.do_test()

    def test_different_merge_build_branches(self):
        self.conf.packages["kwbuildd-test"].git_build_debian_branch = \
            "nightly-debian"

        self.do_test()

        self.assertEqual(self.debian.refs["nightly-debian"].commit,
                         self.debian.refs.debian.commit)


class NightlyBuildTestCase(NightlyTestCaseBase):
    def do_test(self):
        self.daemon.init_repos()
        self.daemon.repositories["kwbuildd-test"].do_nightly_build()

        build_dir = self.conf.get_build_directory()
        arch = deb.dpkg_architecture()["DEB_HOST_ARCH"]

        changes = deb.dcmd(os.path.join(
            build_dir,
            "kwbuildd-test_1.0~~initial-1_" + arch + ".changes")
        )

        files = [os.path.join(build_dir, p) for p in [
            "kwbuildd-test_1.0~~initial-1_" + arch + ".changes",
            "kwbuildd-test_1.0~~initial-1.dsc",
            "kwbuildd-test_1.0~~initial.orig.tar.gz",
            "kwbuildd-test_1.0~~initial-1.debian.tar.xz",
            "kwbuildd-test_1.0~~initial-1_" + arch + ".deb",
        ]]

        changes_file, dsc_file, orig_file, debian_file, deb_file = files

        self.assertEqual(set(files), set(changes))

        with tarfile.open(orig_file, "r:gz") as orig:
            expected_orig_files = {
                "kwbuildd-test-1.0~~initial",
                "kwbuildd-test-1.0~~initial/CMakeLists.txt",
                "kwbuildd-test-1.0~~initial/main.c",
            }

            self.assertEqual(expected_orig_files, set(orig.getnames()))

        with tarfile.open(debian_file, "r:xz") as debian:
            expected_debian_files = {
                "debian",
                "debian/changelog",
                "debian/rules",
                "debian/compat",
                "debian/control",
                "debian/source",
                "debian/source/format",
            }

            self.assertEqual(expected_debian_files, set(debian.getnames()))

        p = subprocess.run(
            ["ar", "p", deb_file, "data.tar.xz"],
            stdout=subprocess.PIPE, check=True,
        )
        with io.BytesIO(p.stdout) as data_tar:
            with tarfile.open(fileobj=data_tar, mode="r:xz") as data:
                expected_data_files = {
                    ".",
                    "./usr",
                    "./usr/bin",
                    "./usr/bin/kwbuildd-test",
                    "./usr/share",
                    "./usr/share/doc",
                    "./usr/share/doc/kwbuildd-test",
                    "./usr/share/doc/kwbuildd-test/changelog.Debian.gz",
                }

                self.assertEqual(expected_data_files,
                                 set(data.getnames()))

    def test_nightly_build(self):
        self.do_test()


class DebTestCase(TestCase):
    def do_test(self, result):
        self.assertEqual("1.0~~initial-1", result["version"])
        self.assertEqual("kwbuildd-test", result["source"])
        self.assertEqual("unstable", result["distribution"])
        self.assertEqual("medium", result["urgency"])

    def test_parsechangelog_pipe(self):
        f = io.StringIO(DEBIAN_CHANGELOG)
        self.do_test(deb.parsechangelog(f))

    def test_parsechangelog_file(self):
        with tempfile.NamedTemporaryFile("w+") as f:
            f.write(DEBIAN_CHANGELOG)
            f.flush()
            self.do_test(deb.parsechangelog(f.name))
